{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE TemplateHaskell       #-}
module TreeTraversal where

import           Control.Lens
import           Data.Maybe   (fromMaybe)

-- * Fixed-Structure Tree

-- The following data types represent a non-recursive tree type, with
-- a maximum depth of 2, and with each type being used at a specific
-- depth.

data Root = Root { _mids :: [Mid] } deriving (Show, Eq)
data Mid = Mid { _leaves :: [Leaf] } deriving (Show, Eq)
data Leaf = Leaf { _leafId :: Int } deriving (Show, Eq)

makeLenses ''Root
makeLenses ''Mid
makeLenses ''Leaf

-- A sum type for the different-depth nodes of the tree.
data SomeNode
  = SomeRoot Root
  | SomeMid Mid
  | SomeLeaf Leaf
  deriving (Show, Eq)

makePrisms ''SomeNode

-- * Focusing Nodes

-- These "focus" types describe a path down the tree of 'Root', 'Mid',
-- and 'Leaf' types. In my real code these are persisted to disk, so
-- they are not getter/setter functions or lenses.

-- | Describes the currently focused 'Mid', and possibly focused 'Leaf'
-- further down.
data MidFocus = MidFocus Int (Maybe LeafFocus)

-- | Describes the currently focused 'Leaf'.
data LeafFocus = LeafFocus Int

-- NOTE: There must always be a focused node, so we don't need a
-- 'RootFocus' type.


-- * Tree traversal over 'SomeNode' (unlawful!)

class TreeTraversal node focus where
  focusing :: focus -> Traversal' node SomeNode

instance TreeTraversal Root MidFocus where
  focusing focus f = case focus of
    MidFocus idx Nothing    -> mids . ix idx %%~ wrappedIn _SomeMid f
    MidFocus idx (Just sub) -> mids . ix idx . focusing sub %%~ f

instance TreeTraversal Mid LeafFocus where
  focusing (LeafFocus idx) f = leaves . ix idx %%~ wrappedIn _SomeLeaf f

wrappedIn :: Prism' s a -> Lens' a s
wrappedIn p f x = fromMaybe x . preview p <$> f (x ^. re p)

-- * Tree traversal over specific node types (lawful)

class TreeTraversal' parent node focus where
  focusing' :: focus -> Traversal' parent node

instance TreeTraversal' Root Mid MidFocus where
  focusing' focus f = case focus of
    MidFocus idx Nothing -> mids . ix idx %%~ f
    MidFocus _ (Just _)  -> pure

instance TreeTraversal' Root Leaf MidFocus where
  focusing' focus f = case focus of
    MidFocus _ Nothing      -> pure
    MidFocus idx (Just sub) -> mids . ix idx . focusing' sub %%~ f

instance TreeTraversal' Mid Leaf LeafFocus where
  focusing' (LeafFocus idx) f = leaves . ix idx %%~ f
