{-# LANGUAGE TemplateHaskell #-}

import           Control.Monad
import           Hedgehog
import           Hedgehog.Checkers.Lens.Properties
import qualified Hedgehog.Gen                      as Gen
import qualified Hedgehog.Range                    as Range
import           System.Exit

import           TreeTraversal

genTree :: Gen Root
genTree = Root <$> Gen.list (Range.linear 1 10) genMid

genMid :: Gen Mid
genMid = Mid <$> Gen.list (Range.linear 1 10) genLeaf

genLeaf :: Gen Leaf
genLeaf = Leaf <$> Gen.int Range.linearBounded

genSomeNode :: Gen SomeNode
genSomeNode =
  Gen.choice [SomeRoot <$> genTree, SomeMid <$> genMid, SomeLeaf <$> genLeaf]

genSomeNodeFn :: Gen (SomeNode -> SomeNode)
genSomeNodeFn = const <$> genSomeNode

-- * Properties


-- These are unlawful (because of 'wrappedIn'):

prop_wrappedIn_SomeRoot_valid_lens =
  property $ isLens (wrappedIn _SomeRoot) genSomeNode genTree genSomeNodeFn

prop_wrappedIn_SomeMid_valid_lens =
  property $ isLens (wrappedIn _SomeMid) genSomeNode genMid genSomeNodeFn

prop_wrappedIn_SomeLeaf_valid_lens =
  property $ isLens (wrappedIn _SomeLeaf) genSomeNode genLeaf genSomeNodeFn

prop_focusing_valid_traversal = property $ do
  isTraversal (focusing (MidFocus 0 Nothing)) genSomeNode genTree genSomeNodeFn
  isTraversal (focusing (MidFocus 0 (Just (LeafFocus 0)))) genSomeNode genTree genSomeNodeFn


-- These traversals are lawful:

prop_focusing'_Mid_valid_traversal = property $
  isTraversal (focusing' (MidFocus 0 Nothing)) genMid genTree (const <$> genMid)

prop_focusing'_Leaf_valid_traversal = property $
  isTraversal (focusing' (MidFocus 0 (Just (LeafFocus 0)))) genLeaf genTree (const <$> genLeaf)



main :: IO ()
main = do
  res <- checkParallel $$discover
  unless res exitFailure
